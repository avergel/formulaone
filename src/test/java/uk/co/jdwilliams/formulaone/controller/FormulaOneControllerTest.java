package uk.co.jdwilliams.formulaone.controller;

import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;
import uk.co.jdwilliams.formulaone.dto.JsonDocument;
import uk.co.jdwilliams.formulaone.dto.JsonElement;
import uk.co.jdwilliams.formulaone.service.ScraperService;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class FormulaOneControllerTest {

    @Mock
    private ScraperService scraperService;

    @InjectMocks
    private FormulaOneController controller;

    @Test
    public void greetingTest() throws IOException {
        //given
        JsonElement driver = new JsonElement("driver", 10);
        JsonElement team = new JsonElement("team", 100);
        JsonDocument json = new JsonDocument(Arrays.asList(driver), Arrays.asList(team));
        Model model = new ConcurrentModel();

        //when
        Mockito.when(scraperService.scrapPage()).thenReturn(json);
        String response = controller.greeting(model);

        //then
        assertEquals(new Gson().toJson(json), ((ConcurrentModel) model).get("jsonRanking"));
        assertEquals("/index", response);
    }

    @Test
    public void greetingShouldReturnErrorOnExceptionTest() throws IOException {
        //when
        Mockito.when(scraperService.scrapPage()).thenThrow(new IOException(""));
        String response = controller.greeting(new ConcurrentModel());

        //then
        assertEquals("/error", response);

    }
}