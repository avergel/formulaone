package uk.co.jdwilliams.formulaone.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JsonDocument {
    List<JsonElement> drivers;
    List<JsonElement> teams;

}
