package uk.co.jdwilliams.formulaone.service;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;
import uk.co.jdwilliams.formulaone.dto.JsonDocument;
import uk.co.jdwilliams.formulaone.dto.JsonElement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ScraperService {

    private final String url = "https://www.f1-fansite.com/f1-results/2018-f1-championship-standings/";

    public JsonDocument scrapPage() throws IOException {
        JsonDocument json = new JsonDocument();
        Document doc = Jsoup.connect(url).get();

        List<JsonElement> drivers = scrapElements(doc, "msr_season_stats", "msr_col1", "msr_col11");
        json.setDrivers(drivers.stream()
                               .sorted(Comparator.comparingInt(JsonElement::getScore).reversed())
                               .collect(Collectors.toList()));
        List<JsonElement> teams = scrapElements(doc, "msr_season_team_results", "msr_team", "msr_total");
        json.setTeams(teams.stream()
                           .sorted(Comparator.comparingInt(JsonElement::getScore).reversed())
                           .collect(Collectors.toList()));
        return json;
    }

    private List<JsonElement> scrapElements(Document document, String tableCssClass, String nameCssClass, String scoreCssClass) {
        Element table = document.getElementsByClass(tableCssClass).first();
        List<JsonElement> elements = new ArrayList<>();
        Elements rows = table.selectFirst("tbody").select("tr");

        for (Element row : rows) {
            if (!row.getElementsByClass(nameCssClass).isEmpty()) {
                JsonElement element = new JsonElement();
                element.setName(row.getElementsByClass(nameCssClass).first().selectFirst("a").text());
                element.setScore(Integer.parseInt(row.getElementsByClass(scoreCssClass).first().text()));
                elements.add(element);
            }
        }

        return elements;
    }
}
