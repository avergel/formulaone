package uk.co.jdwilliams.formulaone.controller;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import uk.co.jdwilliams.formulaone.service.ScraperService;

import java.io.IOException;

@Controller
public class FormulaOneController {

    private final ScraperService scraperService;

    @Autowired
    public FormulaOneController(ScraperService scraperService) {
        this.scraperService = scraperService;
    }

    @GetMapping("/")
    public String greeting(Model model) {
        try {
            String json = new Gson().toJson(scraperService.scrapPage());
            model.addAttribute("jsonRanking", json);

        } catch (IOException e) {
            return "/error";
        }

        return "/index";
    }

}
