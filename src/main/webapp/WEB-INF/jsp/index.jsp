<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <style>
        table {
            border-collapse: collapse;
        }

        table, td, th {
            border: 1px solid black;
            padding: 5px;
        }
        th {
            background: #DDDDDD;
        }
    </style>

    <script type="text/javascript">

        var json = ${jsonRanking};

        function createTable(type, data, maxResults) {
            var table = "<table><thead><th>" + type + "</th><th>Score</th></thead><tbody>"
            for (var i =0; i < maxResults && i < data.length; i++) {
                table += '<tr>';
                table += '<td>' + data[i]['name'] + '</td>';
                table += '<td>' + data[i]['score'] + '</td>';
                table += '</tr>';
            }
            table += "</tbody></table>";
            return table;
        }

    </script>
</head>

<body>
<h1>Drivers</h1>
<script type="text/javascript">
    document.write(createTable("Driver", json["drivers"], 10));
</script>

<h1>Teams</h1>
<script type="text/javascript">
    document.write(createTable("Team", json["teams"], 5));
</script>
</body>
</html>