# Formula 1 Technical Exercise

## Build and run
This project contains the Spring Boot dependency for maven. To build and run the project (assuming you already have maven installed) 
you can execute `mvn spring-boot:run` from the root of the project. Then open a browser and go to `http://localhost:8080` for view the solution
